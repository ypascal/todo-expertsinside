import React, { Component } from 'react';
import AddNewTask from './components/AddNewTask'
import TaskList from './components/TaskList'

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css

import './App.css';

const uuid      = require('uuid/v4')
const _STORE    = localStorage

class App extends Component {

  constructor() {
  	super() 
  	this.state = {
  		tasks: []
  	}

  }

  addNewTask (newTask) {
    newTask.uuid = uuid()

    try {_STORE.setItem(newTask.uuid, JSON.stringify(newTask))}
      catch(e) { console.log ('Storage error in addNewTask(). uuid was: '+newTask.uuid) }

    let tasks = this.state.tasks
        tasks.push (newTask)

    this.setState (tasks)
  }

  updateTaskStatus (thisStatus){
    let tasks = this.state.tasks

    tasks.map (thisTask => {
        if (thisTask.uuid === thisStatus.uuid) {
          thisTask.isDone = thisStatus.isDone
          try {_STORE.setItem(thisTask.uuid, JSON.stringify(thisTask))}
            catch(e) { console.log ('Storage error in updateTaskStatus(). uuid was: '+thisTask.uuid) }
        }
        return null
      })

    this.setState ({tasks:tasks})
  }

  changeActiveFilter (event){
  }

  purgeClick (event) {
    var donetasks = this.state.tasks
                              .filter (thisTask => {
                                      if (thisTask.isDone === true) return true
                                      return false
                                    })
                              .length
                                
    if (donetasks > 0 ) {      
                            
      confirmAlert({
        title: 'Please confirm',
        message: 'Are you sure you want to purge ALL the completed tasks?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {

              let tasks = this.state.tasks
                                    .filter (thisTask => {
                                      if (thisTask.isDone !== true) return true
                                      return false
                                    })

              for(var currentKey in _STORE) {
                  var thisTask = JSON.parse(_STORE.getItem(currentKey))

                  if ((thisTask != null) && (thisTask.isDone === true)) {
                    try {_STORE.removeItem(currentKey)}
                      catch (e) { console.log ('Something weird happened in loading storage in purgeClick(). uuid was: '+currentKey) }
                  }
                }//for

              this.setState ({tasks:tasks})
            }
          },
          {
            label: 'No',
            onClick: () => {}
          }
        ]
      })//confirmAlert

    }

  }

  componentWillMount (event) {
    console.log ('Obviously, the "clear all data after closing" option has to be disabled in your browser, for storage to work as intended...')

    var tasks = []

    for(var currentKey in _STORE) {
      try { var thisTask = JSON.parse(_STORE.getItem(currentKey)) }
        catch (e) { console.log ('Something weird happened in loading storage in componentWillMount(). Hash was: '+currentKey) }
      
      if (thisTask != null)
        tasks.push (thisTask)
      }//for
    
    this.setState ({tasks:tasks}) 
  }
  
  render() {

    return (
      <div className="App">
        <div className="app-wrapper">
          <h1>ToDo App</h1>
          <AddNewTask addNewTask={this.addNewTask.bind(this)} />

          <TaskList tasks={this.state.tasks} 
                    changeTaskStatus={this.updateTaskStatus.bind(this)} 
                    changeActiveFilter={this.changeActiveFilter.bind(this)} 
                    purgeTaskClick={this.purgeClick.bind(this)} />
        </div>          
      </div>
    );
  }
}

export default App;
