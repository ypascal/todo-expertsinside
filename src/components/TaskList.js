import React, { Component } from 'react';
import Task from './Task'
import TaskListFilterSelect from './TaskListFilterSelect'
import PurgeTasks from './PurgeTasks'
import './TaskList.css'

class TaskList extends Component {

  constructor() {
    super() 
    this.state = {
      activeFilter : undefined
    }
 
  }

  changeTaskStatus(thisStatus){
    
    this.setState ( this.state,
              function() {
                this.props.changeTaskStatus(thisStatus)
              })
  }

  changeActiveFilter (selectedFilter){
    this.state.activeFilter = selectedFilter // how can one change the state ???
    this.setState ( this.state,
              function() {
                this.props.changeActiveFilter(selectedFilter)
              })
  }

  purgeClick () {
    this.setState ( this.state,
          function() {
            this.props.purgeTaskClick()
          })
  }

  render() {
    if (this.props.tasks) {
      var taskListItems  = this.props.tasks
                                .filter   (thisTaskListItem => {
                                    return thisTaskListItem.isDone === this.state.activeFilter || this.state.activeFilter === undefined
                                  })
                                .map      (thisTaskListItem => {
                                    //console.log ('this.state.activeFilter = ' + this.state.activeFilter)
                                    return (
                                      <Task key={thisTaskListItem.uuid} 
                                            task={thisTaskListItem} 
                                            changeTaskStatus={this.changeTaskStatus.bind(this)}  />
                                      ) 
                                  })                                  
    }

    return (
      <div className="TaskList">
        <h3>Current Task List</h3>
        <div className="ToolBar">
          <TaskListFilterSelect className="TaskListFilterSelect"
                                activeFilter={this.changeActiveFilter.bind(this)}  />
          <PurgeTasks className="PurgeTasks"
                      purgeClick={this.purgeClick.bind(this)} />
        </div>
        <div className="Tasks">
          {taskListItems}
        </div>
      </div>
    );
  }
}

export default TaskList;
