import React, { Component } from 'react';
import './AddNewTask.css'

class AddNewTask extends Component {

	addNewTaskClick (event) {
		var newTask = {
    	    		taskName: this.refs.addNewTaskMemo.value,
    	    		isDone: false
    	    	}

		if (this.refs.addNewTaskMemo.value !== '') {
    		this.setState (
    			{newTask},
    	    	function (){
    	    		//console.log (this.state)//34:31
    	    		this.props.addNewTask(newTask)
    	    	}
    	    )

			this.refs.addNewTaskMemo.value = ''
		}

		event.preventDefault()
	}

  	render() {
	    return (
	      	<div className="AddNewTask">
      		
		      	<form onSubmit={this.addNewTaskClick.bind(this)}>
		      		{/*<input 	type="text"
		      				className="taskDescription"
		      				ref="addNewTaskMemo" />*/}
		      		<textarea className="taskDescription"
		      					defaultValue="Enter your task description here..."
		      					ref="addNewTaskMemo">
		      					</textarea>		
		      		<input 	type="submit" 
		      				className="submitBtn"
		      				value="Add Task" />
		      	</form>
		      	<div className="clearFormFloats"></div>
	      	</div>

	    )
  	}
}

export default AddNewTask;