import React, { Component } from 'react';
import './PurgeTasks.css'

class PurgeTasks extends Component {

  purgeClick (event) {
    this.setState ( this.state,
          function() {
            this.props.purgeClick()
          })
    event.preventDefault()
  }

  render() {


    return (
      <div className="PurgeTasks">
		<a href="#Purge" 
			className="PurgeTasksButton"
			onClick={this.purgeClick.bind(this)}> 
			Purge completed tasks
		</a> 
      </div>
    );
  }
}

export default PurgeTasks;