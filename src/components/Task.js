import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css'
import './Task.css'

class Task extends Component {

	changeTaskStatus (event){
		var newTaskStatus = {}
			newTaskStatus.taskName =  this.props.task.taskName
		    newTaskStatus.uuid     =  this.props.task.uuid
		    newTaskStatus.isDone   = !this.props.task.isDone

    	this.setState ( {newTaskStatus},
    					function() {
    						this.props.changeTaskStatus(newTaskStatus)
    					})
    	event.preventDefault()
	}

  	render() {
	  	if (this.props.task.isDone) {
	  		return ( 	
	  				<li className="Task">
	  					<div className="taskPlate">
	  						<a href="#task" 
		  						className="todoItemCheckbox done"
		  						onClick={this.changeTaskStatus.bind(this)}> 
		  						<i className="fa fa-check-square"></i>
	  						</a> 
	  						<div className="todoItemText done">
			       				{this.props.task.taskName} 	  							
	  						</div>
	       				</div>
	       				<div className="taskItemSeparator"></div>	
  					</li>
	  				)
	  		
	  	}else{
	  		return ( 	
	  				<li className="Task">
	  					<div className="taskPlate">
		  					<a href="#task" 
		  						className="todoItemCheckbox"
		  						onClick={this.changeTaskStatus.bind(this)}>
		  						<i className="fa fa-square"></i>
		  						
		  					</a> 
	  						<div className="todoItemText">
			       				{this.props.task.taskName} 	  							
	  						</div>		
	       				</div>
	       				<div className="taskItemSeparator"></div>	
  					</li>
	  				)
	  	}

  	}
}

export default Task;
