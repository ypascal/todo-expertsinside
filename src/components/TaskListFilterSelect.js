import React, { Component } from 'react';
import './TaskListFilterSelect.css'

class TaskListFilterSelect extends Component {

  static defaultProps = {
    filterList : [
        {
          optionName  : 'All tasks',
          filterValue : undefined
        }, 
        {
          optionName  : 'Done only',
          filterValue : true
        },
        {
          optionName  : 'Active only',
          filterValue : false
        }
    ]
  }


  taskStatusChange(event) {
    var selectedFilter = this.props.filterList.filter(element => {
        return element.optionName === this.refs.displayFilter.value
      }
    )
    //console.log (selectedFilter[0].filterValue)
    this.props.activeFilter(selectedFilter[0].filterValue)
    //this.props.changeFilter(this.refs.displayFilter.value)
    event.preventDefault()
  }

  render() {
    let taskStatusOptions = this.props.filterList.map(aStatus => {
      return  <option key={aStatus.optionName} value={aStatus.optionName}> {aStatus.optionName} </option>
      })

    return (
      <div className="TaskListFilterSelect">
        {/*<label>Display : </label> <br/>*/}
        <select className ="FilterSelector"
                onChange={this.taskStatusChange.bind(this)} 
                ref="displayFilter">
          {taskStatusOptions}
        </select>
      </div>
    );
  }
}

export default TaskListFilterSelect;